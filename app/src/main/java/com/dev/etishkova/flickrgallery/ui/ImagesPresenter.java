package com.dev.etishkova.flickrgallery.ui;

import android.content.Context;

import com.dev.etishkova.flickrgallery.helpers.Constants;
import com.dev.etishkova.flickrgallery.helpers.ConversionHelper;
import com.dev.etishkova.flickrgallery.models.GalleryItem;
import com.dev.etishkova.flickrgallery.models.Photo;
import com.dev.etishkova.flickrgallery.models.PostResponse;
import com.dev.etishkova.flickrgallery.network.PhotosApi;
import com.dev.etishkova.flickrgallery.network.Service;

import java.util.ArrayList;
import java.util.Arrays;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;


/**
 * Created by elenatishkova on 17/08/2017.
 */

public class ImagesPresenter implements IImagesPresenter {

    private IGalleryView view;
    private CompositeSubscription subscriptions;

    public ImagesPresenter(Context context) {
        this.subscriptions = new CompositeSubscription();
    }

    @Override
    public void setView(IGalleryView view) {
        this.view = view;
    }

    @Override
    public void getImages() {
        //Clear all subscriptions if there are any ongoing ones, for the cases when we want to refresh if
        //there is a refresh in progress at the moment
        if (subscriptions.hasSubscriptions())
            subscriptions.clear();

        //added interceptor to print response because of the error received
        //TODO: remove later
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        PhotosApi photosApi = retrofit.create(PhotosApi.class);
        Service service = new Service(photosApi);

        Service.GetPhotosListCallBack callBack = new Service.GetPhotosListCallBack(){

            @Override
            public void onSuccess(PostResponse response) {
                if (response == null || response.getPhotos() == null || response.getPhotos().getPhoto() == null || response.getPhotos().getPhoto().length == 0){
                    view.showNoPhotosReturned();
                    return;
                }
                Photo[] photos = response.getPhotos().getPhoto();
                ArrayList<Photo> photosList = new ArrayList<>(Arrays.asList(photos));
                ArrayList<GalleryItem> galleryItems = ConversionHelper.getGalleryItemsFromPhotos(photosList);
                view.showPhotos(galleryItems);
            }

            @Override
            public void onError() {
                view.showError();
            }
        };

        Subscription subscription = service.getPhotosList(callBack);
        subscriptions.add(subscription);
    }
}
