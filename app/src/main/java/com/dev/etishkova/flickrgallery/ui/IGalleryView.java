package com.dev.etishkova.flickrgallery.ui;

import com.dev.etishkova.flickrgallery.models.GalleryItem;

import java.util.ArrayList;

/**
 * Created by elenatishkova on 17/08/2017.
 */

public interface IGalleryView {

    void showPhotos(ArrayList<GalleryItem> galleryItems);

    void showNoPhotosReturned();

    void showError();
}
