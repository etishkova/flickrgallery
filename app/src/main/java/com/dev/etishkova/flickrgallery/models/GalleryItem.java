package com.dev.etishkova.flickrgallery.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Used to forma URL for images.
 * Source: https://github.com/marcogx/FlickrPhotoSearch
 * Created by elenatishkova on 18/08/2017.
 */

public class GalleryItem implements Serializable, Parcelable {

    private String id;
    private String secret;
    private String server;
    private String farm;

    private String ownerName;
    private String datePublished;
    private String dateTaken;
    private String tags;

    public GalleryItem(String id, String secret, String server, String farm, String ownerName, String datePublished, String dateTaken, String tags) {
        this.id = id;
        this.secret = secret;
        this.server = server;
        this.farm = farm;
        this.datePublished = datePublished;
        this.ownerName = ownerName;
        this.dateTaken = dateTaken;
        this.tags = tags;
    }

    public String getId() {
        return id;
    }

    public String getOwnerName(){
        return ownerName;
    }

    public String getPublishedDate(){
        return datePublished;
    }

    public String getTags(){
        return tags;
    }

    public String getDateTaken(){
        return dateTaken;
    }

    public String getUrl() {
        return "http://farm" + farm + ".static.flickr.com/" + server + "/" + id + "_" + secret + ".jpg";
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public GalleryItem createFromParcel(Parcel in) {
            return new GalleryItem(in);
        }

        public GalleryItem[] newArray(int size) {
            return new GalleryItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(secret);
        dest.writeString(server);
        dest.writeString(farm);
        dest.writeString(datePublished);
        dest.writeString(ownerName);
        dest.writeString(dateTaken);
        dest.writeString(tags);
    }

    public GalleryItem(Parcel in){
        this.id = in.readString();
        this.secret = in.readString();
        this.server = in.readString();
        this.farm = in.readString();
        this.datePublished = in.readString();
        this.ownerName = in.readString();
        this.dateTaken = in.readString();
        this.tags = in.readString();
    }
}
