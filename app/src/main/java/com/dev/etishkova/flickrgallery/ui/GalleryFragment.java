package com.dev.etishkova.flickrgallery.ui;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.dev.etishkova.flickrgallery.R;
import com.dev.etishkova.flickrgallery.helpers.GalleryAdapter;
import com.dev.etishkova.flickrgallery.models.GalleryItem;
import com.reginald.swiperefresh.CustomSwipeRefreshLayout;

import java.util.ArrayList;

/**
 * Fragment, displaying the gallery of images
 * Created by elenatishkova on 19/08/2017.
 */

public class GalleryFragment extends Fragment implements IGalleryView {

    //Refresh  listener to refresh images on swipe action
    CustomSwipeRefreshLayout.OnRefreshListener refreshListener = new CustomSwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            loadImages();
        }
    };

    private static final String SEARCH_DETAILS = "search_details";

    private EditText etSearch;
    private TextView tvNoPhotos;
    private CustomSwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView rvRecyclerView;

    private GalleryAdapter adapter;
    private IActionListener loadingListener;
    private ImagesPresenter presenter;

    private boolean shouldLoadPhotos = true;

    public static GalleryFragment newInstance() {
        return new GalleryFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new GalleryAdapter((FlickrActivity) getActivity(), new ArrayList<GalleryItem>());
        if (savedInstanceState != null) {
            ArrayList<GalleryItem> galleryItems = savedInstanceState.getParcelableArrayList(SEARCH_DETAILS);
            if (galleryItems != null) {
                adapter.addAll(galleryItems);
                shouldLoadPhotos = false;
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.search_fragment_layout, container, false);
        findViewsAndSetup(v);

        //if returning from details fragment, just keep the same list until user wants to refresh by swiping
        if (adapter.getItems() != null && !adapter.getItems().isEmpty())
            shouldLoadPhotos = false;
        if (shouldLoadPhotos)
            loadImages();
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.loadingListener = (FlickrActivity) getActivity();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(SEARCH_DETAILS, (ArrayList<? extends Parcelable>) adapter.getItems());
    }

    private void findViewsAndSetup(View v) {
        etSearch = (EditText) v.findViewById(R.id.etSearch);
        etSearch.setSelected(false);
        rvRecyclerView = (RecyclerView) v.findViewById(R.id.rvResult);
        tvNoPhotos = (TextView) v.findViewById(R.id.tvNoPhotos);

        //Swipe to refresh photos
        swipeRefreshLayout = (CustomSwipeRefreshLayout) v.findViewById(R.id.swipeRefresh);
        swipeRefreshLayout.setOnRefreshListener(refreshListener);

        //setup recycler view
        rvRecyclerView.setHasFixedSize(true);
        GridLayoutManager rvLayoutManager = new GridLayoutManager(getActivity(), 2);
        rvRecyclerView.setLayoutManager(rvLayoutManager);
        rvRecyclerView.setAdapter(adapter);

        presenter = new ImagesPresenter(getActivity());
        presenter.setView(this);
    }

    @Override
    public void showPhotos(ArrayList<GalleryItem> galleryItems) {
        loadingListener.hideLoading();
        tvNoPhotos.setVisibility(View.GONE);
        adapter.addAll(galleryItems);
    }

    @Override
    public void showNoPhotosReturned() {
        loadingListener.hideLoading();
        tvNoPhotos.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError() {
        loadingListener.showErrorMessage();
    }

    private void loadImages() {
        tvNoPhotos.setVisibility(View.GONE);
        adapter.clear();
        loadingListener.showLoading();
        presenter.getImages();
    }
}
