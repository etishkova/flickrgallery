package com.dev.etishkova.flickrgallery.network;

import com.dev.etishkova.flickrgallery.helpers.Constants;
import com.dev.etishkova.flickrgallery.models.PostResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by elenatishkova on 17/08/2017.
 */

public interface PhotosApi {

    /**
     * Request to get recent photos, setup to use JSON format
     * request 50 photos per page, JSON format
     * added extras: owner name, date uploaded, date taken, image tags
     * Extras added here are: date photo uploaded, date photo is taken, the name of the owner and tags
     * @return information about recent public photos
     */

    @GET(Constants.SEARCH_RECENT_REQUEST + "&api_key=" + Constants.API_KEY + "&per_page=50&extras=date_upload,date_taken,owner_name,tags&format=json&nojsoncallback=1")
    Observable<PostResponse> getRecentImages();

}
