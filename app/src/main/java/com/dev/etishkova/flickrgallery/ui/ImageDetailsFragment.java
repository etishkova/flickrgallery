package com.dev.etishkova.flickrgallery.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dev.etishkova.flickrgallery.R;
import com.dev.etishkova.flickrgallery.helpers.ConversionHelper;
import com.dev.etishkova.flickrgallery.models.GalleryItem;

/**
 * Fragment displaying the details of particular image
 * Created by elenatishkova on 19/08/2017.
 */

public class ImageDetailsFragment extends Fragment {

    private static final String ITEM = "item_details";

    private GalleryItem galleryItem;

    private ImageView ivImage;
    private TextView tvOwnerName;
    private TextView tvDateUploaded;
    private TextView tvDateTaken;
    private TextView tvTags;

    public static ImageDetailsFragment newInstance(GalleryItem item) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(ITEM, item);
        ImageDetailsFragment detailsFragment = new ImageDetailsFragment();
        detailsFragment.setArguments(bundle);
        return detailsFragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            galleryItem = (GalleryItem) args.getSerializable(ITEM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.image_details_fragment_layout, container, false);
        findViews(v);
        setValues();
        return v;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(ITEM, galleryItem);
    }

    private void findViews(View v) {
        ivImage = (ImageView) v.findViewById(R.id.ivImage);
        tvOwnerName = (TextView) v.findViewById(R.id.tvOwnerName);
        tvDateUploaded = (TextView) v.findViewById(R.id.tvDateUploaded);
        tvDateTaken = (TextView) v.findViewById(R.id.tvDateTaken);
        tvTags = (TextView) v.findViewById(R.id.tvTags);
    }

    private void setValues() {
        if (galleryItem != null) {
            Glide.with(getActivity())
                    .load(galleryItem.getUrl())
                    .into(ivImage);
            if (galleryItem.getOwnerName() != null && !galleryItem.getOwnerName().trim().isEmpty())
                tvOwnerName.setText("Published by " + galleryItem.getOwnerName());
            if (galleryItem.getPublishedDate() != null) {
                String dateUploaded = ConversionHelper.getReadableDateTimeFromUnix(galleryItem.getPublishedDate());
                if (dateUploaded != null && !dateUploaded.trim().isEmpty())
                    tvDateUploaded.setText("Published on " + dateUploaded);
            }
            if (galleryItem.getDateTaken() != null) {
                String dateTaken = ConversionHelper.getReadableTakenDateTime(galleryItem.getDateTaken());
                if (dateTaken != null && !dateTaken.trim().isEmpty())
                    tvDateTaken.setText("Taken on " + dateTaken);
            }
            if (galleryItem.getTags() != null && !galleryItem.getTags().trim().isEmpty())
                tvTags.setText("Tags/\n" + galleryItem.getTags());
        }
    }
}
