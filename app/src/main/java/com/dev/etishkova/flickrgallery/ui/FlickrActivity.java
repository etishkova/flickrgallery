package com.dev.etishkova.flickrgallery.ui;

import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.dev.etishkova.flickrgallery.R;
import com.dev.etishkova.flickrgallery.models.GalleryItem;

import static android.view.View.GONE;

public class FlickrActivity extends AppCompatActivity implements IActionListener {

    public static final String SEARCH = "fragSearch";
    public static final String DETAILS = "fragDetails";

    public static final String CURRENT = "current_fragment";

    private ProgressBar pbProgressBar;
    private AlertDialog alertDialog;

    private Fragment currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewsAndSetup();
        if (savedInstanceState != null) {
            currentFragment = getSupportFragmentManager().getFragment(savedInstanceState, CURRENT);
        } else showSearchFragment();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (currentFragment != null && currentFragment.isAdded())
            getSupportFragmentManager().putFragment(outState, CURRENT, currentFragment);
    }

    @Override
    public void onBackPressed() {
        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            fm.popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    private void findViewsAndSetup() {
        pbProgressBar = (ProgressBar) findViewById(R.id.pbProgressBar);

        //Alert dialog is used to notify user of any errors
        alertDialog = new AlertDialog.Builder(FlickrActivity.this).create();
        alertDialog.setTitle("Error");
        alertDialog.setMessage("Oops. Something went wrong. Let's try reloading by swiping down :-)");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
    }

    @Override
    public void showLoading() {
        pbProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        pbProgressBar.setVisibility(GONE);
    }


    @Override
    public void showErrorMessage() {
        hideLoading();
        alertDialog.show();
    }

    private void showSearchFragment() {
        final GalleryFragment galleryFragment = GalleryFragment.newInstance();
        currentFragment = galleryFragment;
        showFragment(galleryFragment, SEARCH, false);
    }

    public void showDetailsFragment(GalleryItem item) {
        final ImageDetailsFragment detailsFragment = ImageDetailsFragment.newInstance(item);
        currentFragment = detailsFragment;
        showFragment(detailsFragment, DETAILS, true);
    }

    private void showFragment(Fragment fragment, String fragmentTag, boolean addToStack) {
        if (addToStack)
            getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack(null)
                    .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
                    .replace(R.id.flFragmentContainer, fragment, fragmentTag)
                    .commit();
        else
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.flFragmentContainer, fragment, fragmentTag)
                    .commit();
    }
}
