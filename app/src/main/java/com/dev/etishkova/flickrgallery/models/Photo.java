package com.dev.etishkova.flickrgallery.models;

/**
 * Created by elenatishkova on 17/08/2017.
 */

public class Photo
{
    private String tags;

    private String isfamily;

    private String dateupload;

    private String ispublic;

    private String isfriend;

    private String id;

    private String farm;

    private String datetakenunknown;

    private String title;

    private String ownername;

    private String owner;

    private String secret;

    private String server;

    private String datetaken;

    private String datetakengranularity;

    public String getTags ()
    {
        return tags;
    }

    public void setTags (String tags)
    {
        this.tags = tags;
    }

    public String getIsfamily ()
    {
        return isfamily;
    }

    public void setIsfamily (String isfamily)
    {
        this.isfamily = isfamily;
    }

    public String getDateupload ()
    {
        return dateupload;
    }

    public void setDateupload (String dateupload)
    {
        this.dateupload = dateupload;
    }

    public String getIspublic ()
    {
        return ispublic;
    }

    public void setIspublic (String ispublic)
    {
        this.ispublic = ispublic;
    }

    public String getIsfriend ()
    {
        return isfriend;
    }

    public void setIsfriend (String isfriend)
    {
        this.isfriend = isfriend;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getFarm ()
    {
        return farm;
    }

    public void setFarm (String farm)
    {
        this.farm = farm;
    }

    public String getDatetakenunknown ()
    {
        return datetakenunknown;
    }

    public void setDatetakenunknown (String datetakenunknown)
    {
        this.datetakenunknown = datetakenunknown;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getOwnername ()
    {
        return ownername;
    }

    public void setOwnername (String ownername)
    {
        this.ownername = ownername;
    }

    public String getOwner ()
    {
        return owner;
    }

    public void setOwner (String owner)
    {
        this.owner = owner;
    }

    public String getSecret ()
    {
        return secret;
    }

    public void setSecret (String secret)
    {
        this.secret = secret;
    }

    public String getServer ()
    {
        return server;
    }

    public void setServer (String server)
    {
        this.server = server;
    }

    public String getDatetaken ()
    {
        return datetaken;
    }

    public void setDatetaken (String datetaken)
    {
        this.datetaken = datetaken;
    }

    public String getDatetakengranularity ()
    {
        return datetakengranularity;
    }

    public void setDatetakengranularity (String datetakengranularity)
    {
        this.datetakengranularity = datetakengranularity;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [tags = "+tags+", isfamily = "+isfamily+", dateupload = "+dateupload+", ispublic = "+ispublic+", isfriend = "+isfriend+", id = "+id+", farm = "+farm+", datetakenunknown = "+datetakenunknown+", title = "+title+", ownername = "+ownername+", owner = "+owner+", secret = "+secret+", server = "+server+", datetaken = "+datetaken+", datetakengranularity = "+datetakengranularity+"]";
    }
}
