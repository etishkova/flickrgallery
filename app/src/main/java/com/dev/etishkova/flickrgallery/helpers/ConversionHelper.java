package com.dev.etishkova.flickrgallery.helpers;

import android.os.health.TimerStat;
import android.text.format.DateFormat;
import android.util.Log;

import com.dev.etishkova.flickrgallery.models.GalleryItem;
import com.dev.etishkova.flickrgallery.models.Photo;

import java.security.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by elenatishkova on 18/08/2017.
 */

public class ConversionHelper {

    //format in which uploaded and taken dates will be printed
    public static final SimpleDateFormat printableFormat = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");

    /**
     * Helper method to convert from array of photos to and array of Gallery Items
     * If null is passed, returns null
     * If empty ArrayList is passed, returns empty lArrayList
     *
     * @param photos
     * @return
     */
    public static ArrayList<GalleryItem> getGalleryItemsFromPhotos(ArrayList<Photo> photos) {
        if (photos == null)
            return null;

        ArrayList<GalleryItem> galleryItems = new ArrayList<>();
        for (Photo photo : photos) {
            GalleryItem item = new GalleryItem(photo.getId(), photo.getSecret(), photo.getServer(), photo.getFarm(), photo.getOwnername(), photo.getDateupload(), photo.getDatetaken(), photo.getTags());
            galleryItems.add(item);
        }
        return galleryItems;
    }

    /**
     * Converts string to date in a given format
     * If null or empty string are passed, returns itself
     * If unable to convert, will return the same string
     * <p>
     * TODO: add granularity of photo to get more versatile formats
     *
     * @param stringDate
     * @return
     * @throws ParseException
     */
    public static String getReadableTakenDateTime(String stringDate) {
        if (stringDate == null || stringDate.isEmpty())
            return stringDate;

        SimpleDateFormat receivedFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String readableDate = null;

        try {
            Date newDate = receivedFormat.parse(stringDate);
            readableDate = printableFormat.format(newDate);
        } catch (ParseException ex) {
            Log.i("Conversion", "Failed to convert date " + stringDate);
            ex.printStackTrace();
            return stringDate;
        }
        return readableDate;
    }

    /**
     * Converts unix timestamp to readable date format
     * if null of empty string is passed, will returns itself
     * <p>
     * if can't convert, returns the same string
     *
     * @param unixTime
     * @return
     */
    public static String getReadableDateTimeFromUnix(String unixTime) {
        if (unixTime == null || unixTime.isEmpty())
            return unixTime;

        String readableDate = null;
        try {
            Long timestamp = Long.valueOf(unixTime) * 1000;
            Date newDate = new Date(timestamp);
            readableDate = printableFormat.format(newDate);
        } catch (NumberFormatException ex) {
            Log.i("Conversion", "Failed to convert unix timestamp to date " + unixTime);
            ex.printStackTrace();
            return unixTime;
        }
        return readableDate;
    }
}
