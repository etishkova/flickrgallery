package com.dev.etishkova.flickrgallery.ui;

/**
 * Created by elenatishkova on 17/08/2017.
 */

public interface IImagesPresenter {

    void setView(IGalleryView view);

    void getImages();
}
