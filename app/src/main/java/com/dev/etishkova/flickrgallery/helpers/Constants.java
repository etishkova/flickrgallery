package com.dev.etishkova.flickrgallery.helpers;

/**
 * Created by elenatishkova on 17/08/2017.
 */

public class Constants {

    public static final String BASE_URL = "https://api.flickr.com/services/rest/";

    public static final String SEARCH_RECENT_REQUEST = "?method=flickr.photos.getRecent";

    public static final String API_KEY = "274bf7432bc69a97d0eb85b35284f07b";

    public static final String SECRET = "2e0dac2ed8e7e419";

}
