package com.dev.etishkova.flickrgallery.helpers;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dev.etishkova.flickrgallery.R;
import com.dev.etishkova.flickrgallery.models.GalleryItem;
import com.dev.etishkova.flickrgallery.ui.FlickrActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter class fro our gallery recycler view
 * Source: https://github.com/marcogx/FlickrPhotoSearch
 * Created by elenatishkova on 18/08/2017.
 */

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView tvOwnerName;
        public TextView tvPublishedDate;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.ivGalleryItem);
            tvOwnerName = (TextView) itemView.findViewById(R.id.tvOwnerName);
            tvPublishedDate = (TextView) itemView.findViewById(R.id.tvDatePublished);
        }
    }

    private FlickrActivity activity;
    private List<GalleryItem> list = new ArrayList<>();

    public GalleryAdapter(FlickrActivity activity, List<GalleryItem> list) {
        this.activity = activity;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.gallery_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final GalleryItem item = list.get(position);

        ImageView imageView = holder.imageView;
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.showDetailsFragment(item);
            }
        });

        Glide.with(activity)
                .load(item.getUrl())
                .thumbnail(0.2f)
                .into(imageView);


        if (item.getOwnerName() != null && !item.getOwnerName().trim().isEmpty())
            holder.tvOwnerName.setText("Published by: " + item.getOwnerName());

        if (item.getPublishedDate() != null){
            String datePublished = ConversionHelper.getReadableDateTimeFromUnix(item.getPublishedDate());
            if (datePublished != null && !datePublished.trim().isEmpty())
                holder.tvPublishedDate.setText("Published on: " + datePublished);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public List<GalleryItem> getItems(){
        return this.list;
    }

    public void addAll(List<GalleryItem> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void clear(){
        this.list.clear();
        notifyDataSetChanged();
    }
}
