package com.dev.etishkova.flickrgallery.ui;

/**
 * Created by elenatishkova on 19/08/2017.
 */

public interface IActionListener {

    void showLoading();

    void hideLoading();

    void showErrorMessage();
}
