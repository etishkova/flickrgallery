package com.dev.etishkova.flickrgallery.network;

import com.dev.etishkova.flickrgallery.models.PostResponse;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by elenatishkova on 19/08/2017.
 */

public class Service {

    public interface GetPhotosListCallBack {
        void onSuccess(PostResponse response);

        void onError();
    }

    private final PhotosApi networkService;

    public Service (PhotosApi networkService){
        this.networkService = networkService;
    }

    public Subscription getPhotosList(final GetPhotosListCallBack callback){
        return networkService.getRecentImages()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends PostResponse>>() {

                    @Override
                    public Observable<? extends PostResponse> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<PostResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError();
                        unsubscribe();
                    }

                    @Override
                    public void onNext(PostResponse response) {
                        callback.onSuccess(response);
                    }
                });

    }
}
