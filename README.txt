This file describes my approach to the project.

It was not possible for me do everything in one go, so I had to do
some parts of functionality and go back to work onthe project again. I hope this
will be ok. Below is what has been done and what approach I have taken.


On the first load, app will load first page of 50 recently uploaded public photos.
User can press on any image and they will be able to view bigger version of this image and
more details about it. When returning from image details fragment, user will see the
same list they have left. They can refresh layout by swiping down the whole recycler view.
App handles device rotation, fragment states.


1. 17 August - Time spent: around 1.5 hours
Did some research about Flickr API, as I've never used it before, signed up to get API key,
Looked for the best approach to take to be able to involve
some of the latest libraries.
Found and completed this great tutorial online, which guides through similar
scenario, but using different API:

https://www.raywenderlich.com/146804/dependency-injection-dagger-2

Started project development, Formed similar structure for the project as in tutorial above.
I decided to check if I can first get list of recent
photos from Flickr using this API:

https://www.flickr.com/services/api/flickr.photos.getRecent.htm

I added interceptor to the project to be able to check the response properly
and it looks like it is successful.

At this point I've decided not to use dependency injection as described in the guide.
First of all I haven't used it very much before in practice (only from tutorials online),
secondly, in our current project the decision
was made to not do injections, because it could create issues with tracing, finding usages.
Also, general opinion was that it is difficult to do a correct configuration for dependency injection.

2. 18 August: Time spent: around 1.5 hours
I use JSON format for serialization, because I haven't really worked with
serializing XML format and I thought it would be suitable, as Flickr allows
you to get data in JSON format.
Added recyclerview adapter, where images are rendered with some meta information about them: owner name
(for date only placeholder for now). Used this project for reference about how to render
image data on Recycler view. Glide library is used to render images.

3. 19 August: Time spent: around 4.5 hours

Flickr was very slow today. I changed the size of the page to 50
 for fetch less photos and reduced the size of image thumbnail.
I found that there were many timeouts and I had to
add swipe to refresh functionality. For this I used SwipeRefresh library,
which is in sample project. Added rxJava, followed this great tutorial:
https://medium.com/@nurrohman/a-simple-android-apps-with-mvp-dagger-rxjava-and-retrofit-4edb214a66d7,
adapted it for this project.

Changed project structure to use fragments, so one can be used to view the gallery
and second one to see image details. Enhanced GalleryItem model to  collect
some more information about the photos. Added this additional information to Image details fragment

I had a difficulty managing the state of recycler view when returning from details fragment, but
it looks like it is working at the moment. Fragments also maintain their state on Rotation, which means
new photos can only be loaded when user swipes down.

Added some conversion methods to convert dates, some basic formatting.

References:
1. http://pojo.sodhanalibrary.com/ - for models definitions
2. https://github.com/marcogx/FlickrPhotoSearch  - sample flickr project
3. https://www.flickr.com/services/api/ - Flickr API
4. StackOverflow - for help